import { Component, OnInit } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Router } from '@angular/router';

declare var $:any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public username : string = '';
  public password : string = '';
  httpOptions : any;
  data:string;
  url : string = 'www.ayushyaaclinic.com/api/public/';
  loading: boolean = false;
  constructor(public http: Http, public router: Router) { }
  ngOnInit() { 
    if(window.localStorage.getItem('loginData')){
      this.router.navigate(['/dashboard']);
      window.location.reload();
    }
  }
  doLogin(){
    if(this.username == '' || this.password == ''){
      this.showNotification('Please fill the details.',4);
      return;
    }
    if(this.username.length != 10){
      this.showNotification('Please enter 10 digit mobile number.',4);
      return;
    }
    this.data = "phone="+this.username+'&password='+this.password;
    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/x-www-form-urlencoded'
      })
    };
    this.loading = true;
    this.http.post('http://'+this.url+'AdminLogin', this.data, this.httpOptions).map(res => res.json()).subscribe(data => {
      this.loading = false;
      if(!data.error){
        this.showNotification(data.message, 2);
        localStorage.setItem('loginData', JSON.stringify(data.data));
        this.router.navigate(['/dashboard']);
      }else{
        this.showNotification(data.message, 4);
      }
    });
  }

  showNotification(message, color){
    var type = ['','info','success','warning','danger'];

  $.notify({
      icon: "ti-info-alt",
      message: message
    },{
        type: type[color],
        timer: 3000,
        placement: {
            from: 'top',
            align: 'center'
        }
    });
}

}