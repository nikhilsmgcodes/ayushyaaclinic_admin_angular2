import { Component, OnInit } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Router } from '@angular/router';

declare var $:any;

@Component({
  selector: 'app-token',
  templateUrl: './token.component.html',
  styleUrls: ['./token.component.css']
})
export class TokenComponent implements OnInit {
  public patient_name : string = '';
  public gender : string = '';
  public phone_number:string='';
  public dateofbirth:string;
  public address:string=''; 
  public convinient_time:string='';
  httpOptions : any;
  data:string;
  url : string = 'www.ayushyaaclinic.com/api/public/';
  filtertoken : string = '';
  filtertokennumber : string;
  AllCompletedToken:any;
  TodayTokenpending:any;
  Todaycompletedtoken:any;
  CurrenntToken:any;
  start_time:any;
  loading: boolean = false;
  
  constructor(public http: Http, public router: Router) { }
  ngOnInit() {
    if(!window.localStorage.getItem('loginData')){
      this.router.navigate(['/login']);
    }
    this.Completed_Token();
    this.GetTodayToken();
  }

  Completed_Token(){
    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/x-www-form-urlencoded'
      })
    };
    this.loading = true;
    this.http.get('http://'+this.url+'getallcompletedTokenHistory',this.httpOptions).map(res => res.json()).subscribe(data => {
      this.loading = false;
      this.AllCompletedToken=data;
      console.log(this.AllCompletedToken);
    });
  }

  GetTodayToken(){
    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/x-www-form-urlencoded'
      })
    };
    this.loading = true;
    this.http.get('http://'+this.url+'GetAllToken',this.httpOptions).map(res => res.json()).subscribe(data => {
      this.loading = false;
      this.TodayTokenpending=data.pending;
      this.Todaycompletedtoken=data.completed;
      this.CurrenntToken=data.current;
    });
  }

  ONStartTime(Token_number){
    this.data = "Token_number="+Token_number;   
    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/x-www-form-urlencoded'
      })
    };
    this.loading = true;
    this.http.post('http://'+this.url+'UpdateStarttime', this.data, this.httpOptions).map(res => res.json()).subscribe(data =>{
      this.loading = false;
      if(!data.error){
        this.showNotification(data.message, 2);
        this.GetTodayToken();
        this.Completed_Token();
      }else{
        this.showNotification(data.message, 4);
      }        
    });
  }

  ONEndTime(Token_number){
    this.data = "Token_number="+Token_number;   
    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/x-www-form-urlencoded'
      })
    };
    this.loading = true;
    this.http.post('http://'+this.url+'Endtoken', this.data, this.httpOptions).map(res => res.json()).subscribe(data => {
      this.loading = false;
      if(!data.error){
        this.showNotification(data.message, 2);
        this.GetTodayToken();
        this.Completed_Token();
      }else{
        this.showNotification(data.message, 4);
      }       
    });
  }

  next_token(Token_number){
    this.data = "currentToken="+Token_number;   
    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/x-www-form-urlencoded'
      })
    };
    this.loading = true;
    this.http.post('http://'+this.url+'nextToken', this.data, this.httpOptions).map(res => res.json()).subscribe(data => {
      this.loading = false;
      if(!data.error){
        this.showNotification(data.message, 2);
        this.GetTodayToken();
        this.Completed_Token();
      }else{
        this.showNotification(data.message, 4);
      }       
    });
  }

  showNotification(message, color){
    var type = ['','info','success','warning','danger'];

    //var color = Math.floor((Math.random() * 4) + 1);

  $.notify({
      icon: "ti-info-alt",
      message: message
    },{
        type: type[color],
        timer: 3000,
        placement: {
            from: 'top',
            align: 'center'
        }
    });
}

}